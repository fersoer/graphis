#!/usr/bin/env node
const yargs = require("yargs");
const util = require("util");
const path = require("path");
const fs = require("fs");

const exec = util.promisify(require("child_process").exec);

async function createResolversIndexFile(resolversDir) {
  const indexFileString = `
  import fs           from 'fs';\n  \
  import {join}       from 'path';\n \
  import {fileLoader, mergeResolvers } from "merge-graphql-schemas";\n \

  const directory = join(__dirname,'./');\n \
  const resolversArray = fileLoader(directory, { recursive: true });\n \
  const resolversMerged = mergeResolvers(resolversArray);\n \
  export default resolversMerged;`;

  fs.writeFile(`${resolversDir}/index.tsx`, indexFileString, function (err) {
    if (err) throw err;
  });
}

async function createTypesIndexFile(typesDir) {
  const indexFileString = `
  import {join}       from 'path';\n \
  import { fileLoader, mergeTypes } from "merge-graphql-schemas";\n \
  //path to files and merge\n \
  const directory = join(__dirname,'./graphs');\n \
  const typesArray = fileLoader(directory, { recursive: true });\n \
  const typesMerged = mergeTypes(typesArray, { all: true });\n \
  export default typesMerged;`;

  fs.writeFile(`${typesDir}/index.tsx`, indexFileString, function (err) {
    if (err) throw err;
  });
}

async function createSchemeIndexFile(graphqlDir) {
  const indexFileString = `
  import { makeExecutableSchema } from "graphql-tools" \n \;
  import typeDefs from "./types/";\n \
  import resolvers from "./resolvers/";\n \
  const schema = makeExecutableSchema({\n \
    typeDefs,\n \
    resolvers,\n \
  });\n \
  export default schema;`;

  fs.writeFile(`${graphqlDir}/index.tsx`, indexFileString, function (err) {
    if (err) throw err;
  });
}

async function createResolverTestFile(graphqlDir) {
  const indexFileString = `
  const TestResolver = {  \n \
  Query: { \n \
    testQuery: async (_root, _args, _context) => { \n \
      return { \n \
        "response":"ok" \n \
      } \n \
    }, \n \
  }, \n \
  Mutation: { \n \
    testMutation: async (_root, _args, _context) => { \n \
      return { \n \
        "response":"ok" \n \
      } \n \
    }, \n \
  }, \n \
  Subscription: { \n \
      \n \
  }, \n \
}; \n \
module.exports = TestResolver;`;

  fs.writeFile(`${graphqlDir}/testResolver.tsx`, indexFileString, function (
    err
  ) {
    if (err) throw err;
  });
}

async function startProject() {
  const rootDir = path.join(process.cwd(), "src", "api");
  const controllerDir = path.join(rootDir, "controllers");
  const graphqlDir = path.join(rootDir, "graphql");
  const resolversDir = path.join(rootDir, "graphql", "resolvers");
  const typesDir = path.join(rootDir, "graphql", "types");
  const typesGraphsDir = path.join(rootDir, "graphql", "types", "graphs");
  const helpersDir = path.join(rootDir, "helpers");
  const modelsDir = path.join(rootDir, "models");
  try {
    const { stdout, stderr } = await exec(
      `mkdir -p ${controllerDir} \
      && mkdir -p ${resolversDir} \
      && mkdir -p ${typesDir} \
      && mkdir -p ${typesGraphsDir} \
      && mkdir -p ${helpersDir} \
      && mkdir -p ${modelsDir} `
    );

    if (!stderr) {
      createSchemeIndexFile(graphqlDir);
      createTypesIndexFile(typesDir);
      createResolversIndexFile(resolversDir);
      createResolverTestFile(resolversDir);
      console.log("Graphis project was created!");
    }
  } catch (error) {
    console.log(error);
    console.log("Something was wrong trying to created directory tree");
  }
}

const options = yargs
  .usage("Usage: $0 [options]")
  .option("start", {
    alias: "s",
    describe: "Create directory tree",
    type: "string",
  })
  .option("controller", {
    alias: "c",
    describe: "Create a new controller",
    type: "string",
  })
  .option("resolver", {
    alias: "r",
    describe: "Create a new resolver",
    type: "string",
  }).argv;

if (options.hasOwnProperty("start")) {
  startProject();
}
