import Waterline from "waterline";
import path from "path";
import fs from "fs";
import { camelize } from "./utilities/string";

const getDataStore = (store: any) => {
  if (globalThis.datastores.default.adapter.datastores[store]) {
    return globalThis.datastores.default.adapter.datastores[store].manager.pool;
  }
  throw new Error("Datastore doest not exists ");
};

const conn = () => {
  return new Promise(async (resolve, reject) => {
    //set config object
    const config = {
      adapters: {},
      datastores: {},
    };

    try {
      // Instantiate a new instance of the ORM
      const orm = new Waterline();
      if (globalThis.hasOwnProperty("config")) {
        //setup adapters
        for (let x in globalThis.config.adapters) {
          config.adapters[x] = require(globalThis.config.adapters[x].adapter);
          let datastore = globalThis.config.adapters[x];
          datastore.adapter = x;
          config.datastores[x] = datastore;
          // globalThis.logger.warn(`Adapters attempt connected at ${datastore.url}`);
        }
      }

      //looad modules from directory source
      if (process.env.PWD){       
        const modelDirectory = path.join(process.env.PWD,process.env.ROOTDIR, "/api/models");         
        if (fs.existsSync(modelDirectory)) {
          fs.readdirSync(modelDirectory)
            .filter(file => {return file.indexOf(".") !== 0 && file !== "index.ts"})
            .forEach(function (file) {
              const modelFilePath = path.join(modelDirectory, file);
              const model = require(modelFilePath);
              orm.registerModel(model(Waterline));
            });
        } else {
          throw Error(`models directory can't be reached`);
        }
      } else {
        throw Error(`models directory can't be founded`);
      }

      // @ts-ignore
      orm.initialize(config, async (error: any, ontology: any) => {
        if (error) {
          // @ts-ignorer
          globalThis.logger.error("Initialize ORM ERROR: [Adapters error]");
          globalThis.logger.error(error);
          reject(error);
        }
        globalThis.logger.warn(`Adapter is connected `);
        if (ontology) {
          //set app orm
          globalThis.models = ontology.collections;
          globalThis.datastores = ontology.datastores;
          globalThis.waterline = ontology;
          globalThis.getDataStore = getDataStore;
        }

        for (let x in ontology.collections) {
          let modelName = camelize(x);
          modelName = `${modelName.charAt(0).toUpperCase()}${modelName.slice(
            1
          )}`;
          globalThis[modelName] = ontology.collections[x];
        }
        resolve(true);
      });
    } catch (error) {
      globalThis.logger.error(error);
      resolve(error);
    }
  });
};
export default conn;
