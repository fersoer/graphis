import fs from "fs";
import path from "path";
import Http from "http";
import { ApolloServer } from "apollo-server-express";
import { graphqlUploadExpress } from 'graphql-upload';
import { mergeTypeDefs, mergeResolvers } from '@graphql-tools/merge';
import { loadFilesSync } from '@graphql-tools/load-files';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { Express } from "express";

const getContext = () => {
  const policesFile = path.resolve(    
    path.join(process.env.PWD,process.env.ROOTDIR,"/config/polices.ts")
  );
  if (fs.existsSync(policesFile)) {
    globalThis.logger.warn(`Graph polices loaded, enabled `);
    let police = require(policesFile).default;
    return police.default;
  } else {
    globalThis.logger.warn(
      `Graph polices disabled, file does not exist, api is open to all`
    );
    return null;
  }
};

const getResolvers = (graphDirectory) =>{  
  const resolverDirectory = path.join(graphDirectory,"/resolvers");
  const resolversArray    = loadFilesSync(resolverDirectory);
  return mergeResolvers(resolversArray);
}

const getTypes = (graphDirectory) =>{
   //path to files and merge
   const typesDirectory  = path.join(graphDirectory, "/types/graphs");    
   const typesArray = loadFilesSync(typesDirectory, { recursive: true });
   return mergeTypeDefs(typesArray);
}

export default (app: Express, server: Http.Server) => {
  try {
    
    const graphDirectory    = path.join(process.env.PWD,process.env.ROOTDIR,"/api/graphql");
    const resolvers  =  getResolvers(graphDirectory);
    const typeDefs   = getTypes(graphDirectory);
    
    const schema = makeExecutableSchema({
      typeDefs,
      resolvers,
    });

    if (fs.existsSync(graphDirectory)) {
      globalThis.logger.warn('Graph resolvers loaded');
      const apollo = new ApolloServer({
        schema,
        debug: false,
        introspection: process.env.ENV === "production" ? false : true,
        playground: process.env.ENV === "production" ? false : true,
        context: getContext(),
        subscriptions: {
          keepAlive: 3,
          onDisconnect: async (_ws, _context) => {
            globalThis.logger.info("client disconected");
          },
        },
      });

      //apollo middleware
      apollo.applyMiddleware({ app, path:`/${process.env.ENDPOINT}`});
      // apollo suscriptions middleware
      apollo.installSubscriptionHandlers(server);
      //graphql upload middleware
      app.use(graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 100 }))
      
    } else {
      globalThis.logger.warn(
        `Graphql module no initialized, because scheme was not found`
      );
    }
  } catch (error) {
    globalThis.logger.error("CatchError -> %o", error);
  }
};
