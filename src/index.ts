import "reflect-metadata";
import "./lib/logger";
import "./lib/loadenv";

import apiServer from "./lib/apiServer";
import apollo from "./lib/apollo";
import orm from "./lib/orm";
import controllers from "./lib/controllers";

//load config files
const configpath = `${process.env.PWD}/${process.env.ROOTDIR}/config/default.ts`;
globalThis.config = require(configpath).default;
console.log(`==================================================`);
console.log(`Graphis v1.0`);
console.log(`==================================================`);

const graphis = async () => {
  // start the Express server
  return new Promise(async (resolver, reject) => {
    try {
      //set express
      const { app, server } = apiServer();
      //set apollo
      apollo(app, server);
      //set controllers
      controllers(app);
      //set orm
      await orm().catch(error => globalThis.logger.error(error));

      app.listen(globalThis.config.port, () => {
        globalThis.logger.warn(
          `Server started at http://localhost:${globalThis.config.port}`
        );
        return resolver();
      });
    } catch (error) {
      globalThis.logger.error(error);
      return reject(error);
    }
  });
};

export default graphis;
