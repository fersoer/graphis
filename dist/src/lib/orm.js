"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const waterline_1 = __importDefault(require("waterline"));
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const string_1 = require("./utilities/string");
const getDataStore = (store) => {
    if (globalThis.datastores.default.adapter.datastores[store]) {
        return globalThis.datastores.default.adapter.datastores[store].manager.pool;
    }
    throw new Error("Datastore doest not exists ");
};
const conn = () => {
    return new Promise((resolve, reject) => __awaiter(void 0, void 0, void 0, function* () {
        //set config object
        const config = {
            adapters: {},
            datastores: {},
        };
        try {
            // Instantiate a new instance of the ORM
            const orm = new waterline_1.default();
            if (globalThis.hasOwnProperty("config")) {
                //setup adapters
                for (let x in globalThis.config.adapters) {
                    config.adapters[x] = require(globalThis.config.adapters[x].adapter);
                    let datastore = globalThis.config.adapters[x];
                    datastore.adapter = x;
                    config.datastores[x] = datastore;
                    // globalThis.logger.warn(`Adapters attempt connected at ${datastore.url}`);
                }
            }
            //looad modules from directory source
            if (process.env.PWD) {
                const modelDirectory = path_1.default.join(process.env.PWD, process.env.ROOTDIR, "/api/models");
                if (fs_1.default.existsSync(modelDirectory)) {
                    fs_1.default.readdirSync(modelDirectory)
                        .filter(file => {
                        return file.indexOf(".") !== 0 && file !== "index.ts";
                    })
                        .forEach(function (file) {
                        const model = require(path_1.default.join(modelDirectory, file))(waterline_1.default);
                        orm.registerModel(model);
                    });
                }
                else {
                    throw Error(`models directory can't be reached`);
                }
            }
            else {
                throw Error(`models directory can't be founded`);
            }
            // @ts-ignore
            orm.initialize(config, (error, ontology) => __awaiter(void 0, void 0, void 0, function* () {
                if (error) {
                    // @ts-ignorer
                    globalThis.logger.error("Initialize ORM ERROR: [Adapters error]");
                    globalThis.logger.error(error);
                    reject(error);
                }
                globalThis.logger.warn(`Adapter is connected `);
                if (ontology) {
                    //set app orm
                    globalThis.models = ontology.collections;
                    globalThis.datastores = ontology.datastores;
                    globalThis.waterline = ontology;
                    globalThis.getDataStore = getDataStore;
                }
                for (let x in ontology.collections) {
                    let modelName = string_1.camelize(x);
                    modelName = `${modelName.charAt(0).toUpperCase()}${modelName.slice(1)}`;
                    globalThis[modelName] = ontology.collections[x];
                }
                resolve(true);
            }));
        }
        catch (error) {
            globalThis.logger.error(error);
            resolve(error);
        }
    }));
};
exports.default = conn;
