"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const cors_1 = __importDefault(require("cors"));
const helmet_1 = __importDefault(require("helmet"));
const body_parser_1 = __importDefault(require("body-parser"));
const apiServer = () => {
    const app = express_1.default();
    const server = http_1.default.createServer(app);
    server.timeout = 0;
    // set middlewares
    // ----------------------------------------------
    //allow Cors
    app.use(cors_1.default());
    //body parser
    app.use(body_parser_1.default.urlencoded({
        extended: true,
        limit: globalThis.config.upload_limit,
        parameterLimit: 50000,
    }));
    //json parser
    app.use(body_parser_1.default.json({
        verify: (req, _res, buf) => {
            req["rawBody"] = buf;
        },
    }));
    //helment security
    app.use(helmet_1.default({
        contentSecurityPolicy: process.env.NODE_ENV === "production" ? undefined : false,
    }));
    return { app, server };
};
module.exports = apiServer;
