"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const dotenv_1 = __importDefault(require("dotenv"));
const loadenv = () => {
    //set default enviroment
    // ----------------------------------------------
    process.env.ENV = !process.env.ENV ? "dev" : process.env.ENV;
    let envConfig = dotenv_1.default.config();
    let envFile = path_1.default.resolve(`.env.${process.env.ENV}`);
    if (fs_1.default.existsSync(envFile)) {
        try {
            envConfig = dotenv_1.default.parse(fs_1.default.readFileSync(envFile));
            globalThis.logger.debug("Enviroment file loaded ");
            for (const k in envConfig) {
                process.env[k] = envConfig[k];
            }
        }
        catch (error) {
            globalThis.logger.error(`Error try to read Enviroment file [.env.${process.env.ENV}]`);
        }
    }
};
loadenv();
module.exports = loadenv;
