"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const fs_1 = __importDefault(require("fs"));
const path_1 = __importStar(require("path"));
const apollo_server_express_1 = require("apollo-server-express");
const graphql_upload_1 = require("graphql-upload");
// import { fileLoader, mergeResolvers, mergeTypes } from "merge-graphql-schemas";
const merge_1 = require("@graphql-tools/merge");
const load_files_1 = require("@graphql-tools/load-files");
const schema_1 = require("@graphql-tools/schema");
const getContext = () => {
    const policesFile = path_1.default.resolve(path_1.default.join(process.env.PWD, process.env.ROOTDIR, "/config/polices.js"));
    if (fs_1.default.existsSync(policesFile)) {
        globalThis.logger.debug(`Polices enabled with file loaded correctly`);
        globalThis.logger.debug(`${policesFile}`);
        let police = require(policesFile);
        return police.default;
    }
    else {
        globalThis.logger.debug(`Polices disabled filed does not exist, api is open to all`);
        return null;
    }
};
module.exports = (app, server) => {
    try {
        const graphDirectory = path_1.default.join(process.env.PWD, process.env.ROOTDIR, "/api/graphql");
        const resolverDirectory = path_1.join(process.env.PWD, process.env.ROOTDIR, "/api/graphql/resolvers");
        const resolversArray = load_files_1.loadFilesSync(resolverDirectory);
        const resolvers = merge_1.mergeResolvers(resolversArray);
        //path to files and merge
        const typesDirectory = path_1.join(process.env.PWD, process.env.ROOTDIR, "/api/graphql/types/graphs");
        const typesArray = load_files_1.loadFilesSync(typesDirectory, { recursive: true });
        const typeDefs = merge_1.mergeTypeDefs(typesArray);
        const schema = schema_1.makeExecutableSchema({
            typeDefs,
            resolvers,
        });
        if (fs_1.default.existsSync(graphDirectory)) {
            const apollo = new apollo_server_express_1.ApolloServer({
                schema,
                debug: false,
                introspection: process.env.ENV === "production" ? false : true,
                playground: process.env.ENV === "production" ? false : true,
                context: getContext(),
                subscriptions: {
                    keepAlive: 3,
                    onDisconnect: (_ws, _context) => __awaiter(void 0, void 0, void 0, function* () {
                        globalThis.logger.info("client disconected");
                    }),
                },
            });
            //apollo middleware
            apollo.applyMiddleware({ app, path: `/${process.env.ENDPOINT}` });
            // apollo suscriptions middleware
            apollo.installSubscriptionHandlers(server);
            //graphql upload middleware
            app.use(graphql_upload_1.graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 100 }));
        }
        else {
            globalThis.logger.warn(`Graphql module no initialized, because scheme was not found`);
        }
    }
    catch (error) {
        globalThis.logger.error("CatchError -> %o", error);
    }
};
