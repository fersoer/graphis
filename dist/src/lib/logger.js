"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const winston_1 = __importDefault(require("winston"));
const logger = () => {
    globalThis.logger = winston_1.default.createLogger({
        silent: false,
        format: winston_1.default.format.combine(winston_1.default.format.splat(), winston_1.default.format.simple(), winston_1.default.format.colorize(), winston_1.default.format.timestamp({
            format: "YYYY-MM-DDTHH:mm:ss",
        }), winston_1.default.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)),
        transports: [
            new winston_1.default.transports.Console({
                level: process.env.LOG_LEVEL || "debug",
            }),
        ],
    });
};
logger();
module.exports = logger;
