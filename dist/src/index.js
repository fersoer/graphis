"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
require("reflect-metadata");
require("./lib/logger");
require("./lib/loadenv");
const apiServer_1 = __importDefault(require("./lib/apiServer"));
const apollo_1 = __importDefault(require("./lib/apollo"));
//load config files
globalThis.config = require(`${process.env.PWD}/${process.env.ROOTDIR}/config/default.ts`);
console.log(`Graphis v1.0`);
console.log(`----------------------------------------------`);
const graphis = () => __awaiter(void 0, void 0, void 0, function* () {
    // start the Express server
    return new Promise((resolver, reject) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            //set express
            const { app, server } = apiServer_1.default();
            //set apollo
            apollo_1.default(app, server);
            // //set controllers
            // controllers(app);
            // //set orm
            // await orm().catch(error => globalThis.logger.error(error));
            app.listen(globalThis.config.port, () => {
                globalThis.logger.warn(`Server started at http://localhost:${globalThis.config.port}`);
                return resolver(app);
            });
        }
        catch (error) {
            globalThis.logger.error(error);
            return reject(error);
        }
    }));
});
module.exports = graphis;
