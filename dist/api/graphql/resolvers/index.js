"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const merge_graphql_schemas_1 = require("merge-graphql-schemas");
const directory = path_1.join(__dirname, "./");
const resolversArray = merge_graphql_schemas_1.fileLoader(directory, { recursive: true });
const resolversMerged = merge_graphql_schemas_1.mergeResolvers(resolversArray);
exports.default = resolversMerged;
