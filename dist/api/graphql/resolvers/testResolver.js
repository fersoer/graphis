"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tests_1 = require("../../helpers/tests");
const TestResolver = {
    Query: {
        testQuery: tests_1.testQuery,
    },
    Mutation: {
        testMutation: tests_1.testMutation,
    },
};
module.exports = TestResolver;
