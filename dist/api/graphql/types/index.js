"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const merge_graphql_schemas_1 = require("merge-graphql-schemas");
//path to files and merge
const directory = path_1.join(__dirname, "./graphs");
const typesArray = merge_graphql_schemas_1.fileLoader(directory, { recursive: true });
const typesMerged = merge_graphql_schemas_1.mergeTypes(typesArray, { all: true });
exports.default = typesMerged;
