"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
// dynamically include routes (Controller)
// ----------------------------------------------
const controllers = (app) => {
    if (process.env.PWD) {
        let controllersPath = path_1.default.join(process.env.PWD, process.env.ROOTDIR, "/api/controllers");
        if (fs_1.default.existsSync(controllersPath)) {
            fs_1.default.readdirSync(controllersPath).forEach(controller => {
                //set directory path in controllers
                let controllerPath = path_1.default.join(controllersPath, controller);
                if (fs_1.default.lstatSync(controllerPath).isDirectory()) {
                    fs_1.default.readdirSync(controllerPath).forEach(action => {
                        let actionPath = path_1.default.join(controllersPath, controller, action);
                        let actionFunction = require(actionPath).default;
                        //define action method
                        let methods = /post|get|put|delete/;
                        let actionFileName = action
                            .replace(".ts", "")
                            .replace(methods, "")
                            .toLowerCase();
                        let actionName = actionFileName === "index"
                            ? `/${controller}`
                            : `/${controller}/${actionFileName}`;
                        if (actionName === "/index") {
                            actionName = "/";
                        }
                        switch (true) {
                            case action.indexOf("post") === 0:
                                app.post(actionName, actionFunction);
                                break;
                            case action.indexOf("get") === 0:
                                app.get(actionName, actionFunction);
                                break;
                            default:
                                app.all(actionName, actionFunction);
                                break;
                        }
                    });
                }
            });
        }
        else {
            globalThis.logger.warn(`Cannot find the controllers directory, because scheme was no found, ${controllersPath}`);
        }
    }
    //bad request
    app.use((_req, res, _next) => {
        res.status(500).send("Bad request");
    });
};
exports.default = controllers;
