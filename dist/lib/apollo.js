"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const apollo_server_express_1 = require("apollo-server-express");
const graphql_upload_1 = require("graphql-upload");
const merge_1 = require("@graphql-tools/merge");
const load_files_1 = require("@graphql-tools/load-files");
const schema_1 = require("@graphql-tools/schema");
const getContext = () => {
    const policesFile = path_1.default.resolve(path_1.default.join(process.env.PWD, process.env.ROOTDIR, "/config/polices.ts"));
    if (fs_1.default.existsSync(policesFile)) {
        globalThis.logger.warn(`Graph polices loaded, enabled `);
        let police = require(policesFile).default;
        return police.default;
    }
    else {
        globalThis.logger.warn(`Graph polices disabled, file does not exist, api is open to all`);
        return null;
    }
};
const getResolvers = (graphDirectory) => {
    const resolverDirectory = path_1.default.join(graphDirectory, "/resolvers");
    const resolversArray = load_files_1.loadFilesSync(resolverDirectory);
    return merge_1.mergeResolvers(resolversArray);
};
const getTypes = (graphDirectory) => {
    //path to files and merge
    const typesDirectory = path_1.default.join(graphDirectory, "/types/graphs");
    const typesArray = load_files_1.loadFilesSync(typesDirectory, { recursive: true });
    return merge_1.mergeTypeDefs(typesArray);
};
exports.default = (app, server) => {
    try {
        const graphDirectory = path_1.default.join(process.env.PWD, process.env.ROOTDIR, "/api/graphql");
        const resolvers = getResolvers(graphDirectory);
        const typeDefs = getTypes(graphDirectory);
        const schema = schema_1.makeExecutableSchema({
            typeDefs,
            resolvers,
        });
        if (fs_1.default.existsSync(graphDirectory)) {
            globalThis.logger.warn('Graph resolvers loaded');
            const apollo = new apollo_server_express_1.ApolloServer({
                schema,
                debug: false,
                introspection: process.env.ENV === "production" ? false : true,
                playground: process.env.ENV === "production" ? false : true,
                context: getContext(),
                subscriptions: {
                    keepAlive: 3,
                    onDisconnect: (_ws, _context) => __awaiter(void 0, void 0, void 0, function* () {
                        globalThis.logger.info("client disconected");
                    }),
                },
            });
            //apollo middleware
            apollo.applyMiddleware({ app, path: `/${process.env.ENDPOINT}` });
            // apollo suscriptions middleware
            apollo.installSubscriptionHandlers(server);
            //graphql upload middleware
            app.use(graphql_upload_1.graphqlUploadExpress({ maxFileSize: 10000000, maxFiles: 100 }));
        }
        else {
            globalThis.logger.warn(`Graphql module no initialized, because scheme was not found`);
        }
    }
    catch (error) {
        globalThis.logger.error("CatchError -> %o", error);
    }
};
