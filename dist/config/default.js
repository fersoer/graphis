"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    appname: process.env.APPNAME || "API",
    port: process.env.PORT ? Number(process.env.PORT) : 9990,
    endpoint: "api",
    upload_limit: process.env.UPLOAD_LIMIT,
    domain: process.env.PUBLIC_DOMAIN,
    secret_key: process.env.SECRET_KEY,
    stripe: {
        key: process.env.STRIPE_SECRET_KEY,
    },
    conekta: {
        secret_key: process.env.CONEKTA_SECRET_KEY,
    },
    googleMaps: {
        key: process.env.GOOGLE_APIKEY,
    },
    aws: {
        aws_access_key_id: process.env.AWS_ACCESS_KEY_ID,
        aws_secret_access_key: process.env.AWS_SECRET_ACCESS_KEY,
        containers: {
            vehicles: {
                root: "assets.app.bussi.com.mx",
                uri: "https://s3-us-west-2.amazonaws.com",
                path: "assets.app.bussi.com.mx/bussi/motoko/vehicles",
            },
            stations: {
                root: "assets.app.bussi.com.mx",
                uri: "https://s3-us-west-2.amazonaws.com",
                path: "assets.app.bussi.com.mx/bussi/motoko/stations",
            },
            drivers: {
                root: "assets.app.bussi.com.mx",
                uri: "https://s3-us-west-2.amazonaws.com",
                path: "assets.app.bussi.com.mx/bussi/motoko/drivers",
            },
            accounts_companies: {
                root: "assets.app.bussi.com.mx",
                uri: "https://s3-us-west-2.amazonaws.com",
                path: "assets.app.bussi.com.mx/bussi/motoko/accounts_companies",
            },
        },
    },
    redis: {
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
    },
    adapters: {
        default: {
            adapter: "sails-mongo",
            url: `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`,
        },
    },
};
